import inspect
import os

from pkgparser.pypackage.pyobject import PyObject
from pkgparser.pypackage.pypackage import PyPackage
from pkgparser.utilities.package import extract_class_attrs

def test():

    # initialize to get package attributes
    pp = PyPackage(os.environ["TEST_PACKAGE"])

    # parse paths
    submodules = os.environ["TEST_PACKAGE_FUNCTION"].split(".")
    import_path = ".".join(submodules[0:-1])
    function_name = submodules[-1]

    # initialize
    po = PyObject(
        object=getattr(__import__(import_path, fromlist=[function_name]), function_name),
        object_path=os.environ["TEST_PACKAGE_FUNCTION"],
        directory_path=pp.directory_path
    )

    assert set(["attributes", "description", "raises", "returns", "urls"]).issubset(extract_class_attrs(po.docstring).keys())
